#! /usr/bin/env node
import { input } from '@inquirer/prompts';
import { addFile, moveFile } from './util.js';
import emoji from "emoji-node";

let _config = {}; // 配置项
// const mode = await select({
//   message: '请选择模式',
//   choices: [
//     {
//       name: '新增页面',
//       value: 'add',
//       description: '将新增vue文件及配套api、const',
//     },
//     {
//       name: '移动页面路径',
//       value: 'move',
//       description: '将移动vue文件及配套api、const',
//     },
//   ]
// })
if(process.argv && process.argv[2]) {
  const mode = process.argv[2];
  if(mode === "add") {
    _config.pathTo = await input({ message: '请选择目标路径' });
    addFile(_config.pathTo);
  }else if(mode === 'move'){
    _config.pathFrom = await input({ message: '请选择当前路径' });
    _config.pathTo = await input({ message: '请选择目标路径' });
    moveFile(_config.pathFrom, _config.pathTo)
  }else{
    console.log(emoji.get('warning'), `无法识别的命令 ${mode}`)
  }
}else{
  console.log(emoji.get('warning'), `请输入命令`)
}
