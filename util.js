import path, {dirname} from 'path';
import fs from 'fs';
import fileSave from 'file-save';
import { fileURLToPath } from 'url'
import emoji from "emoji-node";

const __dirname = path.resolve();
const __filename = fileURLToPath(import.meta.url)

// 递归创建目录 同步方法
function mkdirsSync(dirname) {
  if (fs.existsSync(dirname)) {
    return true;
  } else {
    if (mkdirsSync(path.dirname(dirname))) {
      fs.mkdirSync(dirname);
      return true;
    }
  }
};
// 获取目录下所有文件
function readFileList(path, filesList=[]) {
  var files = fs.readdirSync(path);
  files.forEach(function (itm, index) {
    var stat = fs.statSync(path + "/" + itm);
    if (stat.isDirectory()) {
    //递归读取文件
    readFileList(path + "/" + itm + "/", filesList)
    } else {
      var obj = {};//定义一个对象存放文件的路径和名字
      obj.path = path;//路径
      obj.fileName = itm//名字
      filesList.push(obj);
    }
  })
}

// 新增文件
export function addFile(pagePath) {
  const splitIndex = pagePath.lastIndexOf("/");
  const jsFilePath = pagePath.slice(0, splitIndex + 1);
  const jsFileName = pagePath.slice(splitIndex + 1, pagePath.length);
  const files = [
    {
      fileName: "index.vue",
      type: "template",
      template: "./template/index.vue",
      path:  path.join(__dirname, "/src/views", pagePath),
    },
    {
      fileName: `${jsFileName}.js`,
      template: "./template/const.js",
      path:  path.join(__dirname, "/src/const", jsFilePath),
    },
    {
      fileName: `${jsFileName}.js`,
      template: "./template/api.js",
      path:  path.join(__dirname, "/src/api", jsFilePath),
    },
  ];
  files.forEach((file) => {
    fs.readFile(
      path.join(dirname(__filename), file.template),
      "utf8",
      async function (err, data) {
        if(file.type === "template") {
          data = data.replace(/\$pagePath/g, jsFilePath);
          data = data.replace(/\$fileName/g, jsFileName);
        }
        //新增文件
        fileSave(path.join(file.path, file.fileName))
        .write(data, "utf8")
        .end("\n")
        .finish(() => {
          console.log(`${path.join(file.path, file.fileName)} 新增成功`, emoji.get('heart'))
        })
      }
    );
  });

}

// 移动文件
export function moveFile(pathFrom, pathTo) {
  const pathFromSplitIndex = pathFrom.lastIndexOf("/");
  const pathToSplitIndex = pathTo.lastIndexOf("/");
  const pathFromConf = {
    splitIndex: pathFromSplitIndex,
    jsFilePath: pathFrom.slice(0, pathFromSplitIndex + 1),
    jsFileName: pathFrom.slice(pathFromSplitIndex + 1, pathFrom.length)
  }
  const pathToConf = {
    splitIndex: pathTo.lastIndexOf("/"),
    jsFilePath: pathTo.slice(0, pathToSplitIndex + 1),
    jsFileName: pathTo.slice(pathToSplitIndex + 1, pathTo.length)
  }
  const files = [
    {
      fileNameFrom: "index.vue",
      fileNameTo: "index.vue",
      type: "template",
      pathFrom:  path.join(__dirname, "/src/views", pathFrom),
      pathTo: path.join(__dirname, "/src/views", pathTo),
    },
    {
      fileNameFrom: `${pathFromConf.jsFileName}.js`,
      fileNameTo: `${pathToConf.jsFileName}.js`,
      template: "./template/const.js",
      pathFrom:  path.join(__dirname, "/src/const", pathFromConf.jsFilePath),
      pathTo:  path.join(__dirname, "/src/const", pathToConf.jsFilePath),
    },
    {
      fileNameFrom: `${pathFromConf.jsFileName}.js`,
      fileNameTo: `${pathToConf.jsFileName}.js`,
      template: "./template/api.js",
      pathFrom:  path.join(__dirname, "/src/api", pathFromConf.jsFilePath),
      pathTo:  path.join(__dirname, "/src/api", pathToConf.jsFilePath),
    },
  ];
  const noExistsFile = files.find(file => !fs.existsSync(path.join(file.pathFrom, file.fileNameFrom)));
  if( noExistsFile ){
    const errToast = "不存在相应路径" + noExistsFile.pathFrom + "\\" + noExistsFile.fileNameFrom + "，请检查"
    console.error(errToast)
    return;
  }

  files.forEach(async file => {
    mkdirsSync(file.pathTo);
    if(file.type === "template") { // vue文件
      let fileList = [];
      readFileList(file.pathFrom, fileList) //获取目录下所有文件路径
      await Promise.all(fileList.map(file2 => {
        return new Promise((resolve, reject) => {
          fs.readFile(
            path.join(file2.path, file2.fileName),
            "utf8",
            function (err, data) {
              data = data.replace(new RegExp(pathFrom,'g'), pathTo);
              //新增文件
              fileSave(path.join(file2.path.replace(file.pathFrom, file.pathTo), file2.fileName))
              .write(data, "utf8")
              .end("\n")
              .finish(() => {
                fs.rm(path.join(file2.path, file2.fileName), { force: true },
                  err => {
                    if(err) {
                      reject(err)
                    }else{
                      resolve();
                    }
                  }
                )
              })
            }
          );
        })
      }))
      deleteFolderRecursive(file.pathFrom)
      deleteFolderHighLevel(file.pathFrom)
    }else{
      fs.rename(
        path.join(file.pathFrom, file.fileNameFrom),
        path.join(file.pathTo, file.fileNameTo),
        err => {
          if(err) {
            throw(err)
          } else{
            clearDir(file.pathFrom);
          }
        }
      )
    }
  })
}

function clearDir(dirname) {
  const files = fs.readdirSync(dirname);
  if(files && files.length === 0) {
    fs.rmdirSync(dirname);
    if(fs.existsSync(path.dirname(dirname))){
      clearDir(path.dirname(dirname))
    }
  }
}


function deleteFolderRecursive(folderPath) {
  //判断文件夹是否存在
  if (fs.existsSync(folderPath)) {
    //读取文件夹下的文件目录，以数组形式输出
    fs.readdirSync(folderPath).forEach((file) => {
      //拼接路径
      const curPath = path.join(folderPath, file);
      //判断是不是文件夹，如果是，继续递归
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        //删除文件或文件夹
        fs.unlinkSync(curPath);
      }
    });
    //仅可用于删除空目录
    fs.rmdirSync(folderPath);
  }
}

function deleteFolderHighLevel(folderPath) {
  let lastSpritIndex;
  let curFolderPath;
  for(let i = 0; i < 2; i++) {
    lastSpritIndex = folderPath.lastIndexOf(path.sep, lastSpritIndex - 1)
    curFolderPath = folderPath.substring(0, lastSpritIndex);
    const files = fs.readdirSync(curFolderPath)
    if(files.length === 0) {
      fs.rmdirSync(curFolderPath);
    }
  }
}