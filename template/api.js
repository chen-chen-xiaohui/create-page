import request from '@/router/axios'

/**
 * 获取分页数据
 */
export function getTableList(query) {
  return request({
    url: '',
    method: 'get',
    params: query
  })
}

/**
 * 新增
 */
export function addApi(data) {
  return request({
    url: '',
    method: 'post',
    data
  })
}

/**
 * 修改
 */
export function editApi(data) {
  return request({
    url: '',
    method: 'put',
    data
  })
}

/**
 * 删除
 */
export function deleteApi(id) {
  return request({
    url: ``,
    method: 'delete',
  })
}