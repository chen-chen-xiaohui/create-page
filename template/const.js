import { lazyLoadGlobalRegion } from "@/util/util";
import { rule } from "@/util/rule";

export const tableOption = {
  border: true,
  index: false,
  stripe: true,
  searchMenuSpan: 4,
  menuWidth: 120,
  menuHeaderAlign: "left",
  menuAlign: "left",
  searchSpan: 6,
  dialogWidth: "406px",
  delBtnIcon: "-",
  editBtn: true,
  editBtnText: "修改",
  editBtnIcon: "-",
  updateBtnText: "确认修改",
  addBtn: true,
  addBtnText: "添加",
  addBtnIcon: "-",
  addTitle: "添加",
  saveBtnText: "确认添加",
  editTitle: "修改",
  cancelBtn: false,
  dialogMenuPosition: "center",
  dialogCustomClass: "footer_center_dialog",
  column: [
    {
      type: 'input',
      label: '名称',
      prop: 'name',
      maxlength: 20,
      search: true,
      rules: [{
        required: true,
        message: "输入名称",
        trigger: "blur"
      }],
      span: 24,
    },
    {
      label: '区县',
      prop: 'regionName',
      addDisplay: false,
      editDisplay: false,
    },
    {
      type: "cascader",
      label: "区县",
      prop: "regionCode",
      hide: true,
      placeholder: '选择区县',
      span: 24,
      multiple: false,
      lazy: true,
      checkStrictly: false,
      lazyLoad: lazyLoadGlobalRegion(2, ""),
      rules: [{
        required: true,
        message: "选择区县",
        trigger: "blur"
      }],
    },
    {
      label: '面积',
      prop: 'area',
      append: "亩",
      maxlength: 10,
      placeholder: '输入面积',
      span: 24,
      rules: [{
        required: true,
        message: "输入面积",
        trigger: "blur"
      },{
        required: true,
        validator: rule.validatorDecimalFixedTwo,
        trigger: "blur"
      },],
    },
    {
      label: '日期',
      prop: 'finishDate',
      type: 'date',
      format: 'yyyy-MM-dd',
      placeholder: '选择日期',
      span: 24,
      rules: [{
        required: true,
        message: "选择日期",
        trigger: "change"
      }],
    },
    {
      label: '时间',
      prop: 'createdAt',
      type: 'datetime',
      addDisplay: false,
      editDisplay: false,
      format: 'yyyy-MM-dd HH:mm'
    },
    {
      label: '状态',
      prop: 'status',
      search: true,
      type: "select",
      slot: true,
      dicUrl: "/admin/dict/type/",
    },
  ]
}