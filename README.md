## 介绍

在当前项目中新建或迁移crud主页面及对应const.js、api.js文件，减少重复的页面初始化搭建及迁移工作
<br />

## 环境搭建

```bash
#with yarn
yarn global add zn-create-page

#with npm
npm install zn-create-page -g
```
<br />

## 使用方法

安装依赖包后，在当前项目根目录终端中输入命令

**一、新增**

![Alt text](https://gitee.com/chen-chen-xiaohui/create-page/raw/master/imgs/image-1.png)

填写要生成模板文件路径，项目根目录下将生成模板文件

回车，生成文件效果如图：

![Alt text](https://gitee.com/chen-chen-xiaohui/create-page/raw/master/imgs/image-3.png)

**二、选择迁移：**


该场景需要输入迁移文件原路径及目标路径，这里原路径沿用上一步新增的模板

![Alt text](https://gitee.com/chen-chen-xiaohui/create-page/raw/master/imgs/image-4.png)

回车，生成文件效果如图：

![Alt text](https://gitee.com/chen-chen-xiaohui/create-page/raw/master/imgs/image-6.png)

新建的/path/add路径对应文件已转移到/new-path/edit,且原目录若已成为空目录则删除该目录
<br />

## 文件路径生成规则

vue: /src/views/${path}/index.vue

const: /src/const/${path}.js

api: /src/api/${path}.js